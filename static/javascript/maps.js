var map = L.map('map').fitWorld();

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicmljaHlib3giLCJhIjoiY2ptajI5d3p1MGJjNDNrcG0zcXBjcjA5aCJ9.m_xAKFPxBDJ6nBu_OGctmQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoicmljaHlib3giLCJhIjoiY2ptajI5d3p1MGJjNDNrcG0zcXBjcjA5aCJ9.m_xAKFPxBDJ6nBu_OGctmQ'
}).addTo(map);
map.locate({setView: true, maxZoom: 16});

function onBathRequest(e) {
  if( currentMarker === null) {
    return;
  }
  var latLng = currentMarker.getLatLng();
  var data = {'lat':latLng.lat, 'long': latLng.lng};
  $.post( "/request-bath", data, function( resData ) {
    console.log("shower request succssful");
    alert("bath request success");
  });
}

var currentMarker = null;

function setLocation(e) {
  var popup = L.popup().setContent('<a class="button button-primary bath-btn" href="#">Request a shower</a>');
  if( currentMarker != null) {
    currentMarker.remove();
  }
  currentMarker = L.marker(e.latlng);
  console.log("lat = " + e.latlng.lat + ", long = " + e.latlng.lng);
  currentMarker.addTo(map).bindPopup(popup).openPopup();

  $('.bath-btn').click(onBathRequest);
  $('.search').children('a').attr('href', '/search?lat=' + e.latlng.lat + '&long=' + e.latlng.lng);
}

map.on('locationfound', setLocation);
map.on('click', setLocation);
